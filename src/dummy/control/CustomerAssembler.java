package dummy.control;

import dto.*;
import java.util.*;

public class CustomerAssembler {

    private static CustomerAssembler instance;
    private List<TrafficSummary> trafficSummaries;
    private TravelSummary travelSummary;

    private CustomerAssembler() {
        instance = null;
        travelSummary = null;
    }

    public static CustomerAssembler getInstance() {
        if (instance == null) {
            instance = new CustomerAssembler();
        }
        return instance;
    }

    public Collection<TrafficSummary> retrieveTrafficInformation(TrafficDetail trafficDetail) {
        createDummyList(trafficDetail);
        return trafficSummaries;
    }

    public TravelSummary retrieveTravelSummary(TravelDetail travelDetail) {
        Collection<PassengerDTO> passengers = travelDetail.getPassengers();
        AccountSummary reserver = travelDetail.getReserver();
        TrafficSummary trafficSummary = travelDetail.getTrafficSummary();
        Collection<AbstractVehicle> vehicles = travelDetail.getVehicles();
        double totalPrice = passengers.size() * trafficSummary.getPrice().getPersonPrice();
        long id = (int) (Math.random() * 100000);
        travelSummary = new TravelSummary(reserver, passengers, vehicles, trafficSummary.getId(), trafficSummary.getPrice(), totalPrice, id);
        return travelSummary;
    }

    public ReservationSummary makeReservation(ReservationDetail resDetail) {
        String reservationSerialNumber = "ajas72jqka7q23q";
        String receipt = "Reservation confirmed";
        return new ReservationSummary(reservationSerialNumber, travelSummary, receipt);
    }

    private void createDummyList(TrafficDetail trafficDetail) {
        trafficSummaries = new ArrayList<>();
        String departurePort = trafficDetail.getDeparturePort();
        String destinationPort = trafficDetail.getDestinationPort();
        Date departureTime = trafficDetail.getDepartureTime();
        GregorianCalendar time = new GregorianCalendar();
        time.setTime(departureTime);
        time.set(GregorianCalendar.HOUR_OF_DAY, 6);
        time.set(GregorianCalendar.MINUTE, 0);
        PriceDTO price;
        
        Date arrivalTime = null;
        String ferry = "Ferry 3082Hy";
        for (int i = 0; i < 8; i++) {
            long id = (int) (Math.random() * 100000);
            departureTime = time.getTime();
            if (departurePort.equalsIgnoreCase("Sejero") || destinationPort.equalsIgnoreCase("Sejero")) {
                time.roll(GregorianCalendar.HOUR_OF_DAY, 1);
                arrivalTime = time.getTime();
                time.roll(GregorianCalendar.HOUR_OF_DAY, 2);
                price = new PriceDTO(145.0, 0.0);
            } else {
                time.roll(GregorianCalendar.MINUTE, 20);
                arrivalTime = time.getTime();
                time.roll(GregorianCalendar.MINUTE, 40);
                time.roll(GregorianCalendar.HOUR_OF_DAY, 2);
                price = new PriceDTO(75.0, 0.0);
            }
            TrafficSummary trafficSummary = new TrafficSummary(id, ferry, departurePort, destinationPort, departureTime, arrivalTime, price);
            trafficSummaries.add(trafficSummary);
        }
    }
}
