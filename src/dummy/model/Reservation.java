/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dummy.model;

import dto.AbstractVehicle;
import dto.AccountSummary;
import dto.PassengerDTO;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author dev
 */
public class Reservation {
    
    private static final long serialVersionUID = 1L;
    
    private AccountSummary reserver;
    private Collection<PassengerDTO> passengers;
    private Collection<AbstractVehicle> vehicles;
    private String departurePort;
    private String destinationPort;
    private Date departureTime;
    private Date arrivalTime;
    private double totalPrice;

    public Reservation(AccountSummary reserver, Collection<PassengerDTO> passengers, Collection<AbstractVehicle> vehicles, String departurePort, String destinationPort, Date departureTime, Date arrivalTime, double totalPrice) {
        this.reserver = reserver;
        this.passengers = passengers;
        this.vehicles = vehicles;
        this.departurePort = departurePort;
        this.destinationPort = destinationPort;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
        this.totalPrice = totalPrice;
    }

    public AccountSummary getReserver() {
        return reserver;
    }

    public void setReserver(AccountSummary reserver) {
        this.reserver = reserver;
    }

    public Collection<PassengerDTO> getPassengers() {
        return passengers;
    }

    public void setPassengers(Collection<PassengerDTO> passengers) {
        this.passengers = passengers;
    }

    public Collection<AbstractVehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(Collection<AbstractVehicle> vehicles) {
        this.vehicles = vehicles;
    }

    public String getDeparturePort() {
        return departurePort;
    }

    public void setDeparturePort(String departurePort) {
        this.departurePort = departurePort;
    }

    public String getDestinationPort() {
        return destinationPort;
    }

    public void setDestinationPort(String destinationPort) {
        this.destinationPort = destinationPort;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public Date getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }
    
}
